package com.debug.springboot.server.controller;

import com.debug.springboot.api.enums.StatusCode;
import com.debug.springboot.api.response.BaseResponse;
import com.debug.springboot.server.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 发送邮件controller
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2019/11/7 17:52
 **/
@RestController
@RequestMapping("thread")
public class ThreadController extends AbstractController{

    @Autowired
    private ThreadService threadService;

    @RequestMapping(value = "all/mail/send",method = RequestMethod.GET)
    public BaseResponse sendAllUerEmail(){
        BaseResponse response=new BaseResponse(StatusCode.Success);
        try {
            threadService.sendAllEmailsV1();

        }catch (Exception e){
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "all/insert/data",method = RequestMethod.GET)
    public BaseResponse insertAllData(){
        BaseResponse response=new BaseResponse(StatusCode.Success);
        try {
            threadService.insertDatas();

        }catch (Exception e){
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }
}






























